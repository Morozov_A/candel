# Интернет-магазин свечек

## Добрый день! Вашему вниманию предоставляю мою проектную работу интернет-магазина написанную на Python при помощи Django framework и разных библиотек.


## Цели проекта
- Создать полноценный сайт “интернет-магазин свечей”.
- Применить на практике приобретенные знания в языке программирования Python, посредством создания полноценного web сайта.
- Закрепить полученные навыки в направлении Python Developer.

## Используемые технологии:
- Python 3.5
- Django framework
- Celery
- DB SQLite3
- CSS и JavaScript
- И другие немаловажные библиотеки Python

## Схема приложений
Candel:
- Candels Свечи 
Тут идет описание характеристик свечей
- Cart Корзина, в которой будет производится добавление удаление, информация по корзине.
- User Пользователь,
авторизация и аутентификация пользователя
- Orders Заказы, будет производится подсчет и дальнейшая отправка заказа на согласование.
- Analytic Аналитика, в планах будет хранить на стороне всю информацию касаемо покупок, для анализа и отправления уведомлений, и показа свечек относительно возраста и предпочтений 

## Схемы БД
CandelName - Наименование продукта:
- Body - Тело
- Flavoring - Аромат
- CandlPrices - Цена Свечи
- Color Цвет
- Card Описание
