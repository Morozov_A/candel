from django.shortcuts import render, redirect, get_object_or_404
from django.views.decorators.http import require_POST
from candels.models import CandlName
from .cart import Cart
from .forms import CartAddProductForm


@require_POST
def cart_add(request, product_id):
    cart = Cart(request)
    product = get_object_or_404(CandlName, id=product_id)
    form = CartAddProductForm(request.POST)

    cart.add(product=product,
             quantity=form['quantity'],
             update_quantity=form['update'])

    return redirect('/')

def cart_remove(request, product_id):
    cart = Cart(request)
    product = get_object_or_404(CandlName, name=product_id)
    cart.remove(product)

    return redirect('cart:cart_detail')

def cart_clear(request):
    cart = Cart(request)
    cart.clear()

    return redirect('cart:cart_detail')

def cart_detail(request):
    cart = Cart(request)

    return render(request, 'detail.html', {'cart': cart})
