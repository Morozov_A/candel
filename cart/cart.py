from decimal import Decimal
from django.conf import settings
from candels.models import CandlName


class Cart(object):

    def __init__(self, request):
        """
        Инициализируем корзину
        """

        self.session = request.session
        cart = self.session.get(settings.CART_SESSION_ID)
        if not cart:
            # save an empty cart in the session
            cart = self.session[settings.CART_SESSION_ID] = {}
        self.cart = cart


    def add(self, product, quantity=1, update_quantity=False):
        """
        Добавить продукт в корзину или обновить его количество.
        """

        product_id = str(product.id)
        if product.name not in self.cart.keys():
            self.cart[product.name] = {'price': str(product.candlprices), 'quantity': 1}
        else:
            qty = self.cart[product.name]['quantity']
            self.cart[product.name] = {'price': str(product.candlprices), 'quantity': qty + 1}
            
        self.save()

    def save(self):
        # Обновление сессии cart
        self.session[settings.CART_SESSION_ID] = self.cart
        # Отметить сеанс как "измененный", чтобы убедиться, что он сохранен
        self.session.modified = True

    def remove(self, product):
        """
        Удаление товара из корзины.
        """

        if product.name in self.cart:
            print('Deleted!')
            del self.cart[product.name]
            self.save()

    def __len__(self):
        """
        Подсчет всех товаров в корзине.
        """
        return len(self.cart)

    def get_total(self):
        """
        Подсчет стоимости товаров в корзине.
        """

        total = 0
        for item in self.cart.keys():
            total += float(self.cart[item]['price']) * float(self.cart[item]['quantity'])

        return total

    def clear(self):
        # удаление корзины из сессии
        del self.session[settings.CART_SESSION_ID]
        self.session.modified = True

    def __iter__(self):
        for item in self.cart.items():
            yield item


'''

SERVER << CONNECTION << CLIENT

'''