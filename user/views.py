from django.shortcuts import render
from django.views.generic import CreateView
from .models import MyUser
from .forms import RegisterForm
from django.contrib.auth.views import LoginView, LogoutView

# Create your views here.
class RegisterView(CreateView):
    model = MyUser
    form_class = RegisterForm
    template_name = 'user/register.html'
    success_url = '/'

class AuthView(LoginView):
    template_name = 'user/login.html'