from django.db import models
from django.contrib.auth.models import AbstractUser

# 0 -- std
# 1 -- moder
# 2 -- admin

# Create your models here.
class MyUser(AbstractUser):
    email = models.EmailField(unique=True)
    permissions = models.IntegerField(default=0)
