from django.test import TestCase
from .models import Body, Color, Flavoring

# Create your tests here.
class TestBody(TestCase):

    def setUp(self):
        body = Body.objects.create(name='Геометрическая')
        self.color = Color.objects.create(name='Желтый', body=body)

    def tearDown(self):
        print('Test complited')

    def test_count_empty(self):
        self.assertEqual(self.candlname_count(), 0)

    def test_count_full(self):
        flavoring = Flavoring.objects.create(name=f'Шоколадный')
        self.flavoring.add(flavoring)
        self.save()

        self.assertEqual(self.flavoring_status(), 'One flavor')

