from django.contrib import admin
from .models import CandlName, Body, Flavoring, Color, Card, CandlPrices

# Register your models here.
admin.site.register(Body)
admin.site.register(Flavoring)
admin.site.register(Color)
admin.site.register(CandlName)
admin.site.register(Card)
admin.site.register(CandlPrices)